# xivo-orchestration-service

## Manual Building

Before building, you must check you have the necessaries built tools: 
```
sudo apt-get install devscripts debhelper
```

Then to manually build the package, you need to issue the following command:

```
debuild -us -uc
```

It will generate the deb file in the parent folder of the current working directory. 
On the test environment, you can then install the package manually:

```
dpkg -i xivo-orchestration-service*_all.deb
```
